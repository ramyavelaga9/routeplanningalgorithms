                        /* This contains the implementation of base version for finding the shortest fast path.
                                     connections are processed in increasing order of their departure times
                                         All operations are done inefficiently */
#include<iostream>
#include<vector>
#include<climits>
#include"list.h"
using namespace std;


int List::size()
{
  return list.size();
}

void List :: insertTriple(struct triple T)
{
  list.push_back(T);
}

int List :: getS(int i)
{
  return list[i].s;
}

int List :: getA(int i)
{
  return list[i].a;
}

int List :: getD(int i)
{
  return list[i].d;
}


bool List :: searchTriple(struct triple T)
{
   bool flag=0;
         for(int j=0;j<list.size();j++)
         {
            if(list[j].s==T.s && list[j].a==T.a && list[j].d==T.d)
            {
               flag=1;
               break;
            }
         }
   return flag;
}


bool List :: FindSuitableTriple(int t,struct triple &max)
{
 if(list.size()!=0)
 { 
   max.a=INT_MIN;
    for(int i=0;i<list.size();i++)
    {
        if(list[i].a <= t && max.a < list[i].a)
        {          
             max.s=list[i].s;
             max.a=list[i].a;
             max.d=list[i].d; 
        }
    }
    if(max.a==INT_MIN) return false;
    else return true;
}
else
{
  return false;
}
}
bool List::dominates(struct triple x,struct triple y)
{
  if((x.s > y.s && x.a <= y.a) || ( x.s == y.s && x.a <= y.a && x.d < y.d ) || (x.s == y.s && x.a < y.a && x.d <= y.d ))
  {
    return true;
  }
  else
  {
    return false;
  }
}
bool List::tobeInserted(struct triple T)
{
  for(int i=0;i<list.size();i++)
    {
      if(dominates(list[i],T)==true)
     { 
       return false;
     }
  }
  return true;
} 
bool List :: removeDominatedTriples( )
{
  if(list.size()<2) return false;
  int k=list.size()-1;
  bool flag=0;
  struct triple T=list[k];
    for(int i=0;i<list.size()-1;i++)
    {
       if(dominates(T,list[i])==true)
        {
          list.erase(list.begin()+i);
          i--;
          flag=1;
        } 
    }
    return flag; 
}

void List :: findMinJourneyAndtravel(int &min1,int &min2)
{
  min1=INT_MAX;
  min2=INT_MAX;
  for(int i=0;i<list.size();i++)
  {
       if((list[i].a - list[i].s) < min1)
       {
         min1=list[i].a - list[i].s;
         min2=list[i].d;
       }
      else if((list[i]. a- list[i].s) == min1)
      {
         if(list[i].d <= min2)
         {
             min1=list[i].a - list[i].s;
             min2=list[i].d;
         }
      }
  }
}
        

  
