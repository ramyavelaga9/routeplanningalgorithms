                             /*source and input filename are given as command line arguments.
                 each line of output contains destination vertex,its journey time and its travel time.
                            output is contained in "best_sfp.txt"*/


#include<iostream>
#include<vector>
#include<cassert>
#include<fstream>
#include<string>
#include<cstdlib>
#include<ctime>
#include"list.h"
using namespace std;

struct triple CreateNewTriple(int,int,int);
void readFile(char*,int&,int&,int&,struct connection* &);
void initialise(int,int,int,struct connection*,bool* &,List* &);

int main(int argc,char** argv)
{ 
  int src,vCount,eCount,u,v,t,l,i,a,b,maxDep,x;
  int *journeyTime,*travelTime;
  struct connection *con;
  char* filename;
  bool* counter;
  struct triple T,Tdom;
  bool flag,status=true;
  ofstream fp;
  List* L;
  clock_t start;
  float end;

  assert(argc==3);
  src=atoi(argv[1]);
  filename=argv[2];

  start=clock();
  readFile(filename,vCount,eCount,maxDep,con);

  L=new List[vCount];
  journeyTime=new int[vCount];
  travelTime=new int[vCount];
  
  initialise(eCount,src,maxDep,con,counter,L);
   
  for(i=0;i<eCount;i++)
  {
     u=con[i].start;
     v=con[i].end;
     t=con[i].dep;
     l=con[i].dur;

       if(L[u].FindSuitableTriple(t,T)==true)
       {
            
            if(v==src) 
            {
              continue;
            }
             struct triple Tnew=CreateNewTriple(T.s,t+l,(T.d)+l);

             if(L[v].searchTriple(Tnew)==false &&  L[v].tobeInserted(Tnew)==true)
             {      
               L[v].insertTriple(Tnew);
               L[v].removeDominatedTriples( );
             }
      }
  }
fp.open("best_sfp.txt");
for(i=0;i<vCount;i++)
 {
   L[i].findMinJourneyAndtravel(a,b);
   fp<<i<<" "<<a<<" "<<b<<endl;
 }
 fp.close();
  end=clock()-start;
  cout<<(float)end/CLOCKS_PER_SEC<<endl;
  return 0;
}


void readFile(char* filename,int &Vcount,int &Ecount,int &maxDep,struct connection* &conArray)
{
  ifstream f;
  f.open(filename);
  int i=0;
  f>>Vcount;
  f>>Ecount;
  f>>maxDep;
  conArray=new connection[Ecount];

  while(f>>conArray[i].start)
  {
    f>>conArray[i].end;
    f>>conArray[i].dep;
    f>>conArray[i].dur;
    i++;
  }
 f.close();
}


struct triple CreateNewTriple(int snew,int anew,int dnew)
{
   struct triple Tnew;
   Tnew.s=snew;
   Tnew.a=anew;
   Tnew.d=dnew;
   return Tnew;
}

void initialise(int eCount,int src,int maxDep,struct connection* con,bool* &counter,List* &L)
{
  bool flag;
  int u,v,t,l,i;
  counter=new bool[maxDep];
  for(i=0;i<maxDep;i++)
  {
    counter[i]=false;
  }
  for(i=0;i<eCount;i++)
  {
     u=con[i].start;
     v=con[i].end;
     t=con[i].dep;
     l=con[i].dur;
     if(u==src)
    {           
         counter[t]=true;
    }
  }
   for(i=0;i<=maxDep;i++)
   {
        if(counter[i]==true)
        {
         struct triple Tsrc;
         Tsrc.s=i;
         Tsrc.a=i;
         Tsrc.d=0;
         L[src].insertTriple(Tsrc);
        }
   }
}
