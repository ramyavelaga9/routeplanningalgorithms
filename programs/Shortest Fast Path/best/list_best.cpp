                            /* This contains the implementation of best version for finding the shortest fast path.
                                     connections are processed in increasing order of their arrival times
                                         All operations are done efficiently */


#include<iostream>
#include<vector>
#include<climits>
#include<cassert>
#include<fstream>
#include"list.h"
using namespace std;


int List::size()
{
  return list.size();
}

void List :: insertTriple(struct triple T)
{ 
  list.push_back(T);
}

int List :: getS(int i)
{
  return list[i].s;
}

int List :: getA(int i)
{
  return list[i].a;
}

int List :: getD(int i)
{
  return list[i].d;
}


bool List :: searchTriple(struct triple T)
{
  if(list.size()==0) return false;
   int i=list.size()-1;
   if(list[i].s==T.s && list[i].a==T.a && list[i].d==T.d)
   {
      return true;
   }
   else
   {
     return false;
   }
}


bool List :: FindSuitableTriple(int t,struct triple &max)
{
  int p=list.size();
  int old_p=p;
  while(p>=1 && t<list[p-1].a)
  {
    old_p=p;
    p=p/2;
  }
  if(p>=1 && t>=list[p-1].a)
  {
    int k=binary_search(p-1,old_p-1,t);
    if(k==-1) return false;
    else
    {
      max=list[k];
      return true;
    }
   }
   else
   {
     return false;
   }
}
bool List::dominates(struct triple x,struct triple y) //returns true when x dominates y
{
  if((x.s > y.s && x.a <= y.a) || ( x.s == y.s && x.a <= y.a && x.d < y.d ) || (x.s == y.s && x.a < y.a && x.d <= y.d ))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool List::tobeInserted(struct triple T)
{
    if(list.size()==0) return true;
      int i=list.size()-1;
      if(dominates(list[i],T)==true)
     { 
       return false;
     }
     else return true;
} 

bool List :: removeDominatedTriples( )
{
      if(list.size()<2) return false;
      int i=list.size()-1;
      int t=i-1;
      if(dominates(list[i],list[t])==true)
        {
           list.erase(list.begin()+t);
           return true;
        }
      return false;  
}

void List :: findMinJourneyAndtravel(int &min1,int &min2)
{
  min1=INT_MAX;
  min2=INT_MAX;
  for(int i=0;i<list.size();i++)
  {
       if((list[i].a - list[i].s) < min1)
       {
         min1=list[i].a - list[i].s;
         min2=list[i].d;
       }
      else if((list[i].a - list[i].s) == min1)
      {
         if(list[i].d <= min2)
         {
             min1=list[i].a - list[i].s;
             min2=list[i].d;

         }
      }
  }

} 
int List :: binary_search(int left,int right,int t)
{
  assert(left>=0 && right<=list.size()-1); 
  if(left>right) 
  {
    return -1;
  }
  else if(left==right) 
  {
     if(list[left].a<=t) return left;
     else return -1;
  }
  else
  {
  int mid=(left+right)/2;
  if(list[mid].a<=t && list[mid+1].a>t) return mid;
  else if(list[mid].a<=t && list[mid+1].a<=t) return  binary_search(mid+1,right,t);
  else return binary_search(left,mid-1,t);
  }
}