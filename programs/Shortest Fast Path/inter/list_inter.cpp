                           /* This contains the implementation of intermediate version for finding the Shortest Fast Path.
                                Connections are processed in increasing order of their departure times.
            Finding suitable triple is done efficiently as the triples are arranged in the increasing order of their arrival times.*/

#include<iostream>
#include<vector>
#include<climits>
#include"list.h"
using namespace std;


int List::size()
{
  return list.size();
}
void List :: insertTriple(struct triple T)
{
 bool flag=0;
  int i;
  for(i=0;i<list.size();i++)
  {
    if(list[i].a >= T.a)
    {
       flag=1;
       break;
    }
  }
  if(flag==0)
  {
    list.push_back(T);
  }
  else
  {
    list.insert(list.begin()+i,T);
  }
}

int List :: getS(int i)
{
  return list[i].s;
}

int List :: getA(int i)
{
  return list[i].a;
}

int List :: getD(int i)
{
  return list[i].d;
}


bool List :: searchTriple(struct triple T)
{
         for(int j=0;j<list.size();j++)
         {
            if(list[j].s==T.s && list[j].a==T.a && list[j].d==T.d)
            {
               return true;
            }
         }

   return false;
}


bool List :: FindSuitableTriple(int t,struct triple &max)
{
  if(list.size()==0) return false;
  int k=binary_search(0,list.size()-1,t);
  if(k==-1) return false;
  else 
  {
    max=list[k];
    return true;
  }
}
bool List::dominates(struct triple x,struct triple y)
{
  if((x.s > y.s && x.a <= y.a) || ( x.s == y.s && x.a <= y.a && x.d < y.d ) || (x.s == y.s && x.a < y.a && x.d <= y.d ))
  {
    return true;
  }
  else
  {
    return false;
  }
}
bool List::tobeInserted(struct triple T)
{
  for(int i=0;i<list.size();i++)
    {
      if(dominates(list[i],T)==true)
     { 
       return false;
     }
  }
  return true;
} 

bool List :: removeDominatedTriples(struct triple T)
{
  if(list.size()<2) return false;
  bool flag=0;
    for(int i=0;i<list.size();i++)
    {
       if(dominates(T,list[i])==true)
        {
           list.erase(list.begin()+i);
           i--;
           flag=1;
        } 
    }
    return flag;
}

void List :: findMinJourneyAndtravel(int &min1,int &min2)
{
  min1=INT_MAX;
  min2=INT_MAX;
  for(int i=0;i< list.size();i++)
  {
       if((list[i].a- list[i].s) < min1)
       {
         min1=list[i].a- list[i].s;
         min2=list[i].d;
       }
      else if((list[i].a- list[i].s) == min1)
      {
         if(list[i].d <= min2)
         {
             min1=list[i].a- list[i].s;
             min2=list[i].d;
         }
      }
  }
}
        
int List :: binary_search(int left,int right,int t)
{
  if(left>right) 
  {
    return -1;
  }
  else if(left==right) 
  {
     if(list[left].a<=t) return left;
     else return -1;
  }
  else
  {
  int mid=(left+right)/2;
  if(list[mid].a<=t && list[mid+1].a>t) return mid;
  else if(list[mid].a<=t && list[mid+1].a<=t) return  binary_search(mid+1,right,t);
  else return binary_search(left,mid-1,t);
  }
}
  
