         //This is the header file that conatins only method declarations for intermediate version.
#include<iostream>
#include<vector>
#include<fstream>
using namespace std;
struct triple  //structure of triple which is to be inserted at each vertex
{
   int s;
   int a;
   int d;
};

struct connection //structure of connection that is retrieved from data
{
  int start;
  int end;
  int dep;
  int dur;
};

class List
{
   private:
         vector <triple> list;
   public:
         int size();
         int getS(int);
         int getA(int);
         int getD(int);
         bool searchTriple(struct triple);
         void insertTriple(struct triple);
         int binary_search(int left,int right,int t);
         bool FindSuitableTriple(int,struct triple &T);
         bool dominates(struct triple x,struct triple y);
         bool tobeInserted(struct triple T);
         bool removeDominatedTriples(struct triple T); 
         void findMinJourneyAndtravel(int&,int&);     
};
