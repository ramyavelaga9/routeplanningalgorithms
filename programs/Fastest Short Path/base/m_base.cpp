                       /*source vertex and input filename are given as command line arguments.
                           output is displayed in "base_sfp.txt".
                each line in output file contains destination vertex ,its journey time followed by its travel time */


#include<iostream>
#include<vector>
#include<cassert>
#include<fstream>
#include<string>
#include<cstdlib>
#include<ctime>
#include"list.h"
using namespace std;

struct triple CreateNewTriple(int,int,int);
void readFile(char*,int&,int&,struct connection* &);
void initialise(int,int,struct connection*,List* &);

int main(int argc,char** argv)
{ 
  int src,vCount,eCount,u,v,t,l,i,a,b,m;
  int *journeyTime,*travelTime;
  struct connection *con;
  char* filename;
  struct triple T,Tdom;
  bool flag,status=true;
  ofstream fp;
  List* L;
  clock_t start;
  float end;

  assert(argc==3);
  src=atoi(argv[1]);
  filename=argv[2];

  start=clock();
  readFile(filename,vCount,eCount,con);

  L=new List[vCount];
  journeyTime=new int[vCount];
  travelTime=new int[vCount];
  
  initialise(eCount,src,con,L);

  for(i=0;i<eCount;i++)
  {
     u=con[i].start;
     v=con[i].end;
     t=con[i].dep;
     l=con[i].dur;
       if(L[u].FindSuitableTriple(t,T)==true)
       {
             struct triple Tnew=CreateNewTriple(T.s,t+l,(T.d)+l);
             if(L[v].searchTriple(Tnew)==false &&  L[v].tobeInserted(Tnew)==true)
             {
               L[v].insertTriple(Tnew);
               L[v].removeDominatedTriples( );
             }
       }
   }
  fp.open("base_fsp.txt");
  for(i=0;i<vCount;i++)
  {
     L[i].findMinJourneyAndtravel(a,b);
     fp<<i<<" "<<a<<" "<<b<<endl;
  }
  fp.close();
  end=clock()-start;
  cout<<(float)end/CLOCKS_PER_SEC<<endl;
  return 0;
}


void readFile(char* filename,int &Vcount,int &Ecount,struct connection* &conArray)
{
  int maxDep;
  ifstream f;
  f.open(filename);
  int i=0;
  f>>Vcount;
  f>>Ecount;
  f>>maxDep;
  conArray=new connection[Ecount];

  while(f>>conArray[i].start)
  {
    f>>conArray[i].end;
    f>>conArray[i].dep;
    f>>conArray[i].dur;
    i++;
  }

 f.close();
}


struct triple CreateNewTriple(int snew,int anew,int dnew)
{
   struct triple Tnew;
   Tnew.s=snew;
   Tnew.a=anew;
   Tnew.d=dnew;
   return Tnew;
}

void initialise(int eCount,int src,struct connection* con,List* &L)
{
  bool flag;
  int u,v,t,l,i;
  for(i=0;i<eCount;i++)
  {
     u=con[i].start;
     v=con[i].end;
     t=con[i].dep;
     l=con[i].dur;
     if(u==src)
    {  
         struct triple Tsrc;
         Tsrc.s=t;
         Tsrc.a=t;
         Tsrc.d=0;

        if(L[src].searchTriple(Tsrc)==false)    L[src].insertTriple(Tsrc);
      }
   }
}
