base:list_base.o m_base.o
	g++ -o base list_base.o m_base.o
inter:list_inter.o m_inter.o
	g++ -o inter list_inter.o m_inter.o
best:list_best.o m_best.o
	g++ -o best list_best.o m_best.o

list_base.o:list_base.cpp list.h
	g++ -c list_base.cpp -g
list_inter.o:list_inter.cpp list.h
	g++ -c list_inter.cpp -g
list_best.o:list_best.cpp list.h
	g++ -c list_best.cpp -g

m_base.o:m_base.cpp list.h
	g++ -c m_base.cpp -g
m_inter.o:m_inter.cpp list.h
	g++ -c m_inter.cpp -g
m_best.o:m_best.cpp list.h
	g++ -c m_best.cpp -g

clean:
	rm -rf *.o base inter best
